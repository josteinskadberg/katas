
const fizzBuzz= (n, fizz = 3, buzz = 5) => {
    let fizzCheck = (n) => {if (n % fizz === 0){ return "Fizz"} return ""}
    let buzzCheck = (n) => {if (n % buzz === 0){ return "Buzz"} return ""}
    for (let i = 1; i <= n; i++){
        let out = fizzCheck(i) + buzzCheck(i)
        if (out){
            console.log(out)
        }
        else{
            console.log(i)
        }
    }
}

const fizzBuzzRules = { 
    3 : "Fizz",
    5 : "Buzz"}

const fizzBuzz2= (n, rules = fizzBuzzRules) => {
    for (let i = 1; i <= n; i++){
        out = "" 
        for (rule in rules){
            if (i % rule === 0){
                out += rules[rule]
            } 
        }
        if (out){
            console.log(out)
        }
        else{
            console.log(i)
        }
    }
}