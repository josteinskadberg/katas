

// Double letter splitter  
test = ["Letter", "Really", "Shall", "Mississippi","Easy", "Llama"]

function splitOnDoubleLetter(str, orginal = true){
    let result = []
    let doubleIndex = doubleLetterIndex(str)
    let slice = str.slice(doubleIndex,str.length)
    if (!slice){
        if (!orginal){
            result.push(str)
        }
        return result 
    }
    result.push(str.slice(0,doubleIndex))
    return result.concat(splitOnDoubleLetter(slice, false))

}
function doubleLetterIndex(str){
    str = str.toLowerCase()
    prev = null
    for (let i = 0; i < str.length;i++){
        if (prev == str[i]){
            return i
        }
        prev = str[i]
    }
    return str.length 
}


